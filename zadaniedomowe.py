# Napisz program, który wystawi ocenę z testu.
# * `0 - 39` pkt - ocena niedostateczna 
# * `40 - 54` pkt - ocena dopuszczająca 
# * `55 - 69` pkt - ocena dostateczna 
# * `70 - 84` pkt - ocena dobra 
# * `85 - 98` pkt - ocena bardzo dobra 
# * `99 - 100` pkt - ocena celująca
# 1. Stwórz zmienną `points`, do której będzie przypisany wynik z testu.
# 2. Na początku sprawdź czy ilość punktów jest większa bądź równa `0`, jeśli nie wypisz na stronie komunikat `Ilość punktów mniejsza niż 0.`
# 3. Na początku sprawdź czy ilość punktów jest mniejsza bądź równa `100`, jeśli nie wypisz na stronie komunikat `Ilość punktów większa niż 100.`
# 4. Następnie sprawdź jaka ocena odpowiada danej ilości punktów i wypisz ją na stronie wg. wzoru `Wynik: ocena dobra`
# 5. Rozwiąż to zadanie używając raz konstrukcji `if ... elif ... else`.


def wynik():
    points = int(input("Podaj liczbę zdobytych punktów: "))
    if (points < 0):
        print("Ilość punktów jest mniejsza niż 0")
    elif (points > 100):
        print("Ilość punktów jest większa niż 100")
    elif (points in range(0, 39)):
        print("Twoje ocena to: niedostateczny")
    elif (points in range(40, 54)):
        print("Twoje ocena to: dopuszczający")
    elif (points in range(55, 69)):
        print("Twoje ocena to: dostateczny")
    elif (points in range(70, 84)):
        print("Twoje ocena to: dobry")
    elif (points in range(85, 98)):
        print("Twoje ocena to: bardzo dobry")
    else:
        print("Twoje ocena to: celujący")
    wynik()
wynik()

