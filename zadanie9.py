#napisz program rysujący na podstawie wartości zmiennej `n` następujący schemat:
#dla `n = 5`
#* 2 3 4 5
#* * 3 4 5
#* * * 4 5
#* * * * 5
#* * * * *

#dla `n = 3`
#* 2 3
#* * 3
#* * *

#Do stworzenia takiego zapisu w środku zagnieżdżonej pętli użyj wyrażenia warunkowego `if`
#dzięki któremu zdecydujesz czy wypisać gwiazdkę czy licznik z zagnieżdżonej pętli. (edited)

n = 5
for i in range(0, n):
    output = ""
    for j in range(0, n):
        if i < n:
            output += "1 "
    print(output)
