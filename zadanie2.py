#Zadanie 2
#Stwórz trzy zmienne o nazwach `nr1, nr2, result`.
#1. Do zmiennych `nr1` i `nr2` przypisz liczby odpowiednio `7` i `4`.
#2. Do zmiennej `result` przypisz ich sumę i wypisz ją w konsoli.
#3. Zmiennej `nr1` zmień wartość na `10` ale nie usuwając poprzedniego kodu, pisz w linijkach niżej.
#4. Ponownie oblicz sumę `nr1` i `nr2` i przypisz ją do zmiennej `result`, wypisz wynik w konsoli.
#5. Wynik nie jest taki sam jak w pkt 2, napisz w komentarzu, dlaczego tak się stało.

nr1 = int(7)
nr2 = int(4)
result = nr1 + nr2

print(result)

nr1 = int(10)
result = nr1 + nr2

print(result)
#poniewaz nadpisujemy zmienna nr1
