#W przesłanym kodzie, jest napisany program, który wypisuje w konsoli
#schemat z `n` gwiazdek, taki jak poniżej.
#Przeanalizuj go bardzo dokładnie.
#Przykład dla  ```n = 5```:
#```
#* * * * *
#* * * * *
#* * * * *
#* * * * *
#* * * * *
#```
#Używamy do tego zadania pętli zagnieżdżonych! (edited)


n = 4
for i in range(0, n):
    output = ""
    for j in range(0, n):
        output += "* "
    print(output)
