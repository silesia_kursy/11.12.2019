#Zdefiniuj zmienną x o wartości 16, oraz zmienną y o wartości 15.
#Zdefiniuj zmienną x_y, która będzie przechowywać wartość logiczną porównania czy x < y.
#Zdefiniuj zmienną t o wartości logicznej prawdy, oraz zmienną f o wartości logicznej fałszu.
#Zdefiniuj zmienną t_f, która będzie przechowywać wartość logiczną porównania t LUB f.
#Zdefiniuj zmienną last, która będzie przechowywać wartość logiczną porównania x_y i t_f.
#Wyświetl w konsoli w nowych liniach kolejno zmienne: x_y , t_f, last.
#Przeanalizuj otrzymany wynik.

x = 16
y = 15
x_y = x < y #przyjmuje falsz

t = x > y #prawda True
f = x_y #przyjmuje zmienna ktora przyjmuje falsz False

t_f = t or f #przyjmuje true bo wystarczy jeden true

last = x_y and t_f #przyjmuje false bo obie zmienne nie sa true

print(x_y, t_f, last)
